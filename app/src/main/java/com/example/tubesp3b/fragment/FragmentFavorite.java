package com.example.tubesp3b.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.tubesp3b.MainActivity;
import com.example.tubesp3b.R;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class FragmentFavorite extends Fragment {
    private static final String FILE_NAME = "favor.txt";
    static TextView tv_favorite;
    static ImageButton btn_world;
    private MainActivity activity;

    public FragmentFavorite(MainActivity activity){
        this.activity = activity;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite,container,false);
        tv_favorite = view.findViewById(R.id.tv_bookmark);
        load(view);
        btn_world = view.findViewById(R.id.btn_world_by);
        btn_world.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                activity.pindahFragmentWorld();
            }
        });

        return view;
    }

    public void load(View v) {
        FileInputStream fis = null;

        try {
            fis = getActivity().openFileInput(FILE_NAME);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader br = new BufferedReader((isr));
            StringBuilder sb = new StringBuilder();
            String text_nama;

            while((text_nama = br.readLine())!=null) {
                sb.append(text_nama).append("\n");
            }

            tv_favorite.setText(sb.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
