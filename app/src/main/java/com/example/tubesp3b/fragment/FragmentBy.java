package com.example.tubesp3b.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.tubesp3b.MainActivity;
import com.example.tubesp3b.services.FetchData;
import com.example.tubesp3b.R;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;


import static android.content.Context.MODE_PRIVATE;

public class FragmentBy extends Fragment implements View.OnClickListener {
    private static final String FILE_NAME = "favor.txt";
    private ByViewModel byViewModel;
    static ImageView iv_country_by;
    static EditText et_search_by;
    static TextView tv_from_by;
    static TextView tv_country_name_by;
    static TextView tv_total_by;
    static TextView tv_active_by;
    static TextView tv_death_by;
    static TextView tv_recovered_by;
    static Button btn_search_country;
    static ImageButton btn_world;
    static ImageButton btn_book;
    private MainActivity activity;
    private static int totalCase;

    public FragmentBy(MainActivity activity){
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_by, container, false);
        byViewModel = new ViewModelProvider(this).get(ByViewModel.class);
        byViewModel.init();
//        this.size = byViewModel.getData().getValue().size();
        iv_country_by = view.findViewById(R.id.iv_country_by);
        et_search_by = view.findViewById(R.id.et_search_country_by);
        tv_from_by = view.findViewById(R.id.tv_from_by);
        tv_country_name_by = view.findViewById(R.id.tv_country_name_by);
        tv_total_by = view.findViewById(R.id.tv_total_by);
        tv_active_by = view.findViewById(R.id.tv_active_by);
        tv_death_by = view.findViewById(R.id.tv_death_by);
        tv_recovered_by = view.findViewById(R.id.tv_recovered_by);
        btn_search_country = view.findViewById(R.id.btn_search_by);
        btn_world = view.findViewById(R.id.btn_world_by);
        btn_world.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.pindahFragmentWorld();
            }
        });
        btn_book = view.findViewById(R.id.bookmark);
        btn_book.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                save(v);
            }
        });
        byViewModel = new ViewModelProvider(getActivity()).get(ByViewModel.class);
        byViewModel.init(); // init repo
        btn_search_country.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        MainActivity.TOTAL_ACTIVE = 0;
        MainActivity.TOTAL_RECOVERED = 0;
        MainActivity.TOTAL_DEATH = 0;
        FetchData process = new FetchData();
        process.setFeature("summary");
        process.setCountry(et_search_by.getText().toString());
        process.execute();
    }

    public static void setView(int totalActiveInput, int totalDeathInput, int totalRecoveredInput,String iSO2Input) {
        totalCase = 0;
        MainActivity.TOTAL_ACTIVE = 0;
        MainActivity.TOTAL_RECOVERED = 0;
        MainActivity.TOTAL_DEATH = 0;
        totalCase += totalActiveInput + totalDeathInput + totalRecoveredInput;
        tv_total_by.setText("Total Cases :\n" + totalCase);
        tv_active_by.setText("Total Active Cases :\n" + totalActiveInput);
        tv_death_by.setText("Total Death Cases :\n" + totalDeathInput);
        tv_recovered_by.setText("Total Recovered :\n" + totalRecoveredInput);
        String name = et_search_by.getText().toString();
        tv_country_name_by.setText(name.toUpperCase());
        if(!MainActivity.COUNTRY_NAME.equalsIgnoreCase("")) {
            String link = "https://www.countryflags.io/" + iSO2Input.toLowerCase() + "/shiny/64.png";
            LoadImage loadImage = new LoadImage(iv_country_by);
            loadImage.execute(link);
        }
    }


    private static class LoadImage extends AsyncTask<String,Void, Bitmap> {
        ImageView imageView;
        public LoadImage(ImageView iv_country_by) {
            this.imageView = iv_country_by;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            String url = strings[0];
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new java.net.URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            iv_country_by.setImageBitmap(bitmap);
        }

    }

    public void save(View v) {
        String favorite = et_search_by.getText().toString();

        FileOutputStream fos = null;
        try {
            fos = getActivity().openFileOutput(FILE_NAME, MODE_PRIVATE);
            fos.write(favorite.getBytes());

            et_search_by.getText().clear();
            Toast.makeText(getActivity(),"Saved to " + getActivity().getFilesDir() + "/" + FILE_NAME, Toast.LENGTH_LONG).show();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
