package com.example.tubesp3b.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.tubesp3b.MainActivity;
import com.example.tubesp3b.R;

public class FragmentSetting extends Fragment {
    private MainActivity activity;
    private ImageButton btnWorld;
    private Switch mode;
    private Spinner mSpinner;

    public FragmentSetting(MainActivity activity){
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting,container,false);
        btnWorld = view.findViewById(R.id.btn_world_by);
        btnWorld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.pindahFragmentWorld();
            }
        });

        return view;
    }
}
