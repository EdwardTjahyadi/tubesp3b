package com.example.tubesp3b.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.tubesp3b.MainActivity;
import com.example.tubesp3b.R;
import com.example.tubesp3b.services.FetchData;

public class FragmentWorld extends Fragment {
    private WorldViewModel worldViewModel;
    static int totalCase;
    static TextView tv_total_world;
    static TextView tv_active_world;
    static TextView tv_death_world;
    static TextView tv_recovered_world;
    static ImageButton btn_world;
    private MainActivity activity;

    public FragmentWorld(MainActivity activity){ this.activity = activity;}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_world, container, false);
        tv_total_world = view.findViewById(R.id.tv_total_world);
        tv_active_world = view.findViewById(R.id.tv_active_world);
        tv_death_world = view.findViewById(R.id.tv_death_world);
        tv_recovered_world = view.findViewById(R.id.tv_recovered_world);
        btn_world = view.findViewById(R.id.btn_world);
        btn_world.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.pindahFragmentWorld();
            }
        });
        worldViewModel = new ViewModelProvider(getActivity()).get(WorldViewModel.class);
        worldViewModel.init(); // init repo
        MainActivity.TOTAL_ACTIVE = 0;
        MainActivity.TOTAL_RECOVERED = 0;
        MainActivity.TOTAL_DEATH = 0;
        FetchData process = new FetchData();
        process.setFeature("summary");
        process.setCountry("global");
        process.execute();
        return view;
    }

    public static void setView(int totalActiveInput, int totalDeathInput, int totalRecoveredInput) {
        totalCase = 0;
        totalCase += totalActiveInput + totalDeathInput + totalRecoveredInput;
        System.out.println(totalActiveInput);
        tv_total_world.setText("Total Cases :\n" + totalCase);
        tv_active_world.setText("Total Active Cases :\n" + totalActiveInput);
        tv_death_world.setText("Total Death Cases :\n" + totalDeathInput);
        tv_recovered_world.setText("Total Recovered :\n" + totalRecoveredInput);
    }
}
