package com.example.tubesp3b.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.*;

import com.example.tubesp3b.MainActivity;
import com.example.tubesp3b.R;
import com.example.tubesp3b.adapters.RecyclerViewAdapter;
import com.example.tubesp3b.models.Case;
import java.util.List;

import static android.content.ContentValues.TAG;

public class FragmentRanking extends Fragment{
    static int totalCase;
    private Button btn_lihat_ranking;
    private static RecyclerView recyclerView;
    private static RecyclerViewAdapter adapter;
    private MainActivity activity;
    private RankingViewModel rankingViewModel;
    private ImageButton btn_world;
    private List<Case> list_case;
    public FragmentRanking(MainActivity activity){
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_world_ranking,container,false);
        recyclerView =  view.findViewById(R.id.recycler_view);
        rankingViewModel = new ViewModelProvider(this).get(RankingViewModel.class);
        rankingViewModel.init(); // init repo
        btn_lihat_ranking = view.findViewById(R.id.lihat_ranking);
        list_case = new ArrayList<Case>();
        adapter = new RecyclerViewAdapter(list_case,(MainActivity)getActivity());
        initRecyclerView();
        btn_world = view.findViewById(R.id.btn_world_ranking);
        btn_world.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.pindahFragmentWorld();
            }
        });
        btn_lihat_ranking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list_case.clear();
                list_case.addAll(rankingViewModel.getData().getValue());
                Collections.sort(list_case,Collections.reverseOrder());
                adapter.add(list_case);
            }
        });
        Handler handler=new Handler();
        Runnable r = new Runnable() {
            public void run() {
                Toast.makeText(getActivity(), "Data Fetched!",
                        Toast.LENGTH_LONG).show();
            }
        };
        handler.postDelayed(r, 1200);
        return view;
    }

    public static void setView(int totalActiveInput, int totalDeathInput, int totalRecoveredInput) {
        totalCase = 0;
        totalCase += totalActiveInput + totalDeathInput + totalRecoveredInput;
        System.out.println(totalActiveInput);
    }

    public void initRecyclerView() {
        Log.d(TAG, "initRecyclerView: mulai recyclerviewnya");
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }
}
