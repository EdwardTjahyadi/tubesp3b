package com.example.tubesp3b.fragment;

import android.os.Handler;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.tubesp3b.models.Case;
import com.example.tubesp3b.repositories.CaseRepository;

import java.util.List;

public class RankingViewModel extends ViewModel {
    private MutableLiveData<List<Case>> data;
    private CaseRepository caseRepository;

    public void init() {
        if (data != null) {
            return;
        }
        System.out.println("MASUK KE RANKING VIEW MODEL");
        caseRepository = CaseRepository.getInstance();
        caseRepository.getCases();
        Handler handler=new Handler();
        Runnable r = new Runnable() {
            public void run() {
                //what ever you do here will be done after 5 seconds delay.
                data = caseRepository.mutable;
            }
        };
        handler.postDelayed(r, 1000);
    }

    public LiveData<List<Case>> getData() { return data; }
}
