package com.example.tubesp3b.fragment;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.tubesp3b.repositories.CaseRepository;
import com.example.tubesp3b.models.Case;

import java.util.List;

public class ByViewModel extends ViewModel {
    private MutableLiveData<List<Case>> data;
    private CaseRepository caseRepository;

    public void init() {
        if (data != null) {
            return;
        }
        caseRepository = CaseRepository.getInstance();
        caseRepository.getCases();
        data = caseRepository.mutable;
    }

    public LiveData<List<Case>> getData() {
        return data;
    }
}
