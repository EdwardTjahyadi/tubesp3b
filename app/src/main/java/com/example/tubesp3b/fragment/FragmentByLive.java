package com.example.tubesp3b.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.tubesp3b.MainActivity;
import com.example.tubesp3b.services.FetchData;
import com.example.tubesp3b.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

public class FragmentByLive extends Fragment implements View.OnClickListener{
    private ByLiveViewModel byLiveViewModel;
    static ImageView iv_country_live;
    static EditText et_tanggal_from;
    static EditText et_bulan_from;
    static EditText et_tahun_from;
    static EditText et_tanggal_to;
    static EditText et_bulan_to;
    static EditText et_tahun_to;
    static EditText et_search_live;
    static TextView tv_from_by;
    static TextView tv_country_name_live;
    static TextView tv_total_live;
    static TextView tv_active_live;
    static TextView tv_death_live;
    static TextView tv_recovered_live;
    static Button btn_search_country_live;
    static ImageButton btn_world;
    private MainActivity activity;
    private static int totalCase;

    public FragmentByLive(MainActivity activity){
        this.activity = activity;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_live_by, container, false);
        byLiveViewModel = new ViewModelProvider(this).get(ByLiveViewModel.class);
        byLiveViewModel.init();
        iv_country_live = view.findViewById(R.id.iv_country_live);
        et_search_live = view.findViewById(R.id.et_search_country_live);
        et_tanggal_from = view.findViewById(R.id.et_tanggal_from_live);
        et_bulan_from = view.findViewById(R.id.et_bulan_from_live);
        et_tahun_from = view.findViewById(R.id.et_tahun_from_live);
        et_tanggal_to = view.findViewById(R.id.et_tanggal_to_live);
        et_bulan_to = view.findViewById(R.id.et_bulan_to_live);
        et_tahun_to = view.findViewById(R.id.et_tahun_to_live);
        tv_country_name_live = view.findViewById(R.id.tv_country_name_live);
        tv_active_live = view.findViewById(R.id.tv_active_live);
        btn_search_country_live = view.findViewById(R.id.btn_search_live);
        btn_world = view.findViewById(R.id.btn_world_live);
        btn_world.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.pindahFragmentWorld();
            }
        });
        btn_search_country_live.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        MainActivity.TOTAL_ACTIVE = 0;
        FetchData process = new FetchData();
        process.setFeature("summary_live");
        process.setCountry(et_search_live.getText().toString());
        process.setFrom(et_tanggal_from.getText().toString(),
                et_bulan_from.getText().toString(),
                et_tahun_from.getText().toString());
        process.setTo(et_tanggal_to.getText().toString(),
                et_bulan_to.getText().toString(),
                et_tahun_to.getText().toString());
        process.execute();
    }

    public static void setView(int totalActiveInput, int totalDeathInput, int totalRecoveredInput,String iSO2Input) {
        totalCase = 0;
        MainActivity.TOTAL_ACTIVE = 0;
        totalCase += totalActiveInput + totalDeathInput + totalRecoveredInput;
        tv_active_live.setText("Total Active Cases :\n" + totalActiveInput);
        String name = et_search_live.getText().toString();
        tv_country_name_live.setText(name.toUpperCase());
        if(!MainActivity.COUNTRY_NAME.equalsIgnoreCase("")) {
            String link = "https://www.countryflags.io/" + iSO2Input.toLowerCase() + "/shiny/64.png";
            FragmentByLive.LoadImage loadImage = new FragmentByLive.LoadImage(iv_country_live);
            loadImage.execute(link);
        }
    }

    private static class LoadImage extends AsyncTask<String,Void, Bitmap> {
        ImageView imageView;
        public LoadImage(ImageView iv_country_by) {
            this.imageView = iv_country_by;
        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            String url = strings[0];
            Bitmap bitmap = null;
            try {
                InputStream inputStream = new java.net.URL(url).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            iv_country_live.setImageBitmap(bitmap);
        }

    }
}
