package com.example.tubesp3b.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tubesp3b.MainActivity;
import com.example.tubesp3b.R;
import com.example.tubesp3b.models.Case;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private static final String TAG = "RecycleViewAdapter";

    private List<Case> list_case_ranking;
    private Context context;
    private MainActivity activity;

    public RecyclerViewAdapter(List<Case> list_case_input, MainActivity activity) {
        this.list_case_ranking = list_case_input;
        this.activity = activity;
        if (list_case_input.isEmpty()) {
            this.list_case_ranking = new ArrayList<Case>();
//            Log.d("RVVMNYA : " ,"EMPTY");
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_case,parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: called");
        ((ViewHolder)holder).tv_index.setText(String.valueOf(position+1));
        ((ViewHolder)holder).tv_country_list.setText((list_case_ranking.get(position).getCountryName()));
        int totalCase = list_case_ranking.get(position).getConfirmed() + list_case_ranking.get(position).getConfirmed() + list_case_ranking.get(position).getConfirmed();
        ((ViewHolder)holder).tv_total_list.setText(String.valueOf(totalCase));


//        holder.parent_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d(TAG, "onClick: " + list_nama_menu.get(position));
//                activity.pindahFragmentDetail(list_nama_menu.get(position));
//            }
//        });
    }

//    @Override
//    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
//        Log.d(TAG, "onBindViewHolder: called");
//
//        ((ViewHolder)holder).menu.setText((list_nama_menu.get(position).getNamaMenu()));
//
//        holder.parent_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d(TAG, "onClick: " + list_nama_menu.get(position));
//                activity.pindahFragmentDetail(list_nama_menu.get(position));
//            }
//        });
//    }


    public void add(List<Case> caseInput) {
        this.list_case_ranking.addAll(caseInput);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list_case_ranking.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        int index = 1;
        TextView tv_index;
        TextView tv_country_list;
        TextView tv_total_list;
        LinearLayout parent_layout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_index = itemView.findViewById(R.id.tv_index_list);
            tv_country_list = itemView.findViewById(R.id.tv_country_list);
            tv_total_list = itemView.findViewById(R.id.tv_total_case_list);
            parent_layout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
