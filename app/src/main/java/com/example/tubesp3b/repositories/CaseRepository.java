package com.example.tubesp3b.repositories;

import android.os.AsyncTask;
import android.os.Handler;

import androidx.lifecycle.MutableLiveData;
import com.example.tubesp3b.models.Case;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CaseRepository {
    private static CaseRepository instance;
    private ArrayList<Case> dataSet = new ArrayList<>();
    public MutableLiveData<List<Case>> mutable = new MutableLiveData<>();

    public static CaseRepository getInstance() {
        if(instance == null) {
            instance = new CaseRepository();
        }
        return instance;
    }

    public void getCases() {
        MutableLiveData<List<Case>> data = new MutableLiveData<>();
        if (dataSet.size() == 0) {
            LoadData loadData = new LoadData();
            loadData.execute();
        }
        Handler handler=new Handler();
        Runnable r = new Runnable() {
            public void run() {
                System.out.println("SUDAH DAPAT DATA HARUSNYA");
                data.setValue(dataSet);
                mutable = data;
            }
        };
        handler.postDelayed(r, 1000);
    }



    class LoadData extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                String data = "";
                URL url = new URL("https://api.covid19api.com/summary");
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                while (line != null) {
                    line = bufferedReader.readLine();
                    data = data + line;
                }
                Object JA;
                JA = new JSONObject(data);
                JSONObject jOSummary = (JSONObject) JA;
                JSONArray jOCountries = jOSummary.getJSONArray("Countries");
                for (int i = 0; i < jOCountries.length(); i++) {
                    JSONObject jOTest = jOCountries.getJSONObject(i);
                    dataSet.add(new Case(jOTest.get("Country").toString(),
                            jOTest.get("CountryCode").toString(),
                            jOTest.get("Slug").toString(),
                            jOTest.getInt("TotalConfirmed"),
                            jOTest.getInt("TotalDeaths"),
                            jOTest.getInt("TotalRecovered")));
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
