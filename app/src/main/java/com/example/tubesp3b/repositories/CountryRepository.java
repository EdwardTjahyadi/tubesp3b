package com.example.tubesp3b.repositories;


import androidx.lifecycle.MutableLiveData;
import com.example.tubesp3b.models.Country;
import java.util.ArrayList;
import java.util.List;


public class CountryRepository {

    private static CountryRepository instance;
    private ArrayList<Country> dataSet = new ArrayList<>();

    public static CountryRepository getInstance() {
        if(instance == null) {
            instance = new CountryRepository();
        }
        return instance;
    }

    // disini buat ngambil datanya nanti dari databse atau tempat apapun itu
    public MutableLiveData<List<Country>> getCountries() {
        MutableLiveData<List<Country>> data = new MutableLiveData<>();
        data.setValue(dataSet);
        return data;
    }

    public void setCountries(String countryName, String slug, String iso2) {
        dataSet.add(new Country(countryName,slug,iso2));
    }
}