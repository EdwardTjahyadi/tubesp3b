package com.example.tubesp3b;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


import com.example.tubesp3b.fragment.FragmentBy;
import com.example.tubesp3b.fragment.FragmentByLive;
import com.example.tubesp3b.fragment.FragmentFavorite;
import com.example.tubesp3b.fragment.FragmentHome;
import com.example.tubesp3b.fragment.FragmentRanking;
import com.example.tubesp3b.fragment.FragmentSetting;
import com.example.tubesp3b.fragment.FragmentWorld;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import com.google.android.material.navigation.NavigationView;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public  static TextView data;
    public static String COUNTRY_NAME;
    public static String SLUG;
    public static String COUNTRY_CODE;
    public static int TOTAL_ACTIVE;
    public static int TOTAL_DEATH;
    public static int TOTAL_RECOVERED;
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.nav_open
                ,R.string.nav_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new FragmentHome()).commit();
        }
    }


    public static void changeBy() {
        FragmentBy.setView(TOTAL_ACTIVE,TOTAL_DEATH,TOTAL_RECOVERED,COUNTRY_CODE);
    }

    public static void changeByLive() {
        FragmentByLive.setView(TOTAL_ACTIVE,TOTAL_DEATH,TOTAL_RECOVERED,COUNTRY_CODE);
    }

    public static void changeWorld() {
        FragmentWorld.setView(TOTAL_ACTIVE,TOTAL_DEATH,TOTAL_RECOVERED);
    }

    public static void changeRanking() {
        FragmentRanking.setView(TOTAL_ACTIVE,TOTAL_DEATH,TOTAL_RECOVERED);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()) {
            case R.id.nav_by:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new FragmentBy(this)).commit();
                closeSoftKeayboard();
                break;
            case R.id.nav_live:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new FragmentByLive(this)).commit();
                closeSoftKeayboard();
                break;
            case R.id.nav_ranking:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new FragmentRanking(this)).commit();
                closeSoftKeayboard();
                break;
            case R.id.nav_favor:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new FragmentFavorite(this)).commit();
                closeSoftKeayboard();
                break;
            case R.id.nav_setting:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        new FragmentSetting(this)).commit();
                closeSoftKeayboard();
                break;
            case R.id.nav_exit:
                System.exit(0);
        }

        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public void closeSoftKeayboard() {
        View focusedView = this.getCurrentFocus();
        InputMethodManager imm = (InputMethodManager) this.getSystemService(this.getApplicationContext().INPUT_METHOD_SERVICE);
        if (imm.isAcceptingText() && focusedView != null) {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void pindahFragmentWorld() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new FragmentWorld(this)).commit();
        closeSoftKeayboard();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }
}