package com.example.tubesp3b.models;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Case implements Comparable<Case> {
    private Integer totalCases;
    private String countryName;
    private String countryCode;
    private String slug;
    private int confirmed;

    public Integer getTotalCases() {
        return totalCases;
    }

    public void setTotalCases(Integer totalCases) {
        this.totalCases = totalCases;
    }

    private int recovered;
    private int deaths;
    private Date date;

    public Case(String countryName, String countryCode, String slug, int confirmed, int recovered, int deaths) {
        this.countryName = countryName;
        this.countryCode = countryCode;
        this.slug = slug;
        this.confirmed = confirmed;
        this.recovered = recovered;
        this.deaths = deaths;
        this.totalCases = this.confirmed;
        //this.date = date;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(int confirmed) {
        this.confirmed = confirmed;
    }

    public int getRecovered() {
        return recovered;
    }

    public void setRecovered(int recovered) {
        this.recovered = recovered;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    public String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }


    @Override
    public int compareTo(Case o) {
        return this.getTotalCases().compareTo(o.getTotalCases());
    }


    public void setDate(Date date) {
        this.date = date;
    }
}
