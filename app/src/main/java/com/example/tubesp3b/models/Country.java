package com.example.tubesp3b.models;

public class Country {
    private String countryName;
    private String slug;
    private String iso2;

    public Country(String countryName, String slug, String iso2) {
        this.countryName = countryName;
        this.slug = slug;
        this.iso2 = iso2;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getIso2() {
        return iso2;
    }

    public void setIso2(String iso2) {
        this.iso2 = iso2;
    }
}
